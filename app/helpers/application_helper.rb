module ApplicationHelper
  def current_user
    @user = User.find_by(uid: session[:user_id]) if session[:user_id]
  end
end
