class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ApplicationHelper
  def require_logged_in
    unless current_user
      redirect_to "/auth/sso"
    end
  end

  

  def login user
    session[:user_id] = user.uid
  end
end
