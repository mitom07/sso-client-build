require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Sso < OmniAuth::Strategies::OAuth2
      # Give your strategy a name.
      option :name, "sso"

      # This is where you pass the options you would pass when
      # initializing your consumer from the OAuth gem.
      option :client_options, {
        :site => ENV["PROVIDER_URL"],
        :authorize_url => "#{ENV["PROVIDER_URL"]}/auth/sso/authorize",
        :access_token_url => "#{ENV["PROVIDER_URL"]}/auth/sso/get_access_token"
      }

      # These are called after authentication has succeeded. If
      # possible, you should try to set the UID without making
      # additional calls (if the user id is returned with the token
      # or as a URI parameter). This may not be possible with all
      # providers.
      uid{ raw_info['id'] }

      info do
        {
          :email => raw_info["info"]['email']
        }
      end

      def raw_info
        binding.pry
        @raw_info ||= access_token.get("/auth/sso/get_info_user.json?oauth_token=#{access_token.token}").parsed
      end
    end
  end
end
